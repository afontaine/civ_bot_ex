# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :civ_bot_ex,
  namespace: CivBot,
  ecto_repos: [CivBot.Repo]

# Configures the endpoint
config :civ_bot_ex, CivBotWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "qzJnXMHsqYUtXUVdEO1RUltrZtR215afx8Pwl2B5xQuNHpHtTpClonzW4Wi9gi0U",
  render_errors: [view: CivBotWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: CivBot.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :ueberauth, Ueberauth,
  providers: [
    discord:
      {Ueberauth.Strategy.Discord,
       [
         default_scope: "identify",
         prompt: "none"
       ]}
  ]

config :ueberauth, Ueberauth.Strategy.Discord.OAuth,
  client_id: System.get_env("DISCORD_CLIENT_ID"),
  client_secret: System.get_env("DISCORD_CLIENT_SECRET"),
  serializers: %{"application/json" => Jason}

config :oauth2,
  serializers: %{"application/json" => Jason}

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
