FROM elixir:1.9
RUN apt-get update && apt-get install -y locales \
  && export LANG=en_US.UTF-8 \
  && echo $LANG UTF-8 > /etc/locale.gen \
  && locale-gen \
  && update-locale LANG=$LANG
RUN apt-get install -y inotify-tools
RUN apt-get install -y apt-transport-https \
  && curl -sL https://deb.nodesource.com/setup_12.x | bash \
  && apt-get install -y nodejs
RUN mix local.hex --force
RUN mix local.rebar --force
ADD . /app
WORKDIR /app
RUN mix do deps.get, deps.compile
RUN cd assets && npm i
RUN mix compile
RUN mix phx.gen.cert
CMD elixir --name civ_bot@webapp --cookie development --erl '-kernel inet_dist_listen_min 9000' --erl '-kernel inet_dist_listen_max 9000' -S mix phx.server
