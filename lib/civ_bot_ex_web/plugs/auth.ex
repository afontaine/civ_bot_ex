defmodule CivBotWeb.Plugs.Auth do
  @behaviour Plug

  def init(_) do
    :ok
  end

  def call(conn, _opts) do
    user = Plug.Conn.get_session(conn, :current_user)

    conn
    |> Plug.Conn.assign(:current_user, user)
  end
end
