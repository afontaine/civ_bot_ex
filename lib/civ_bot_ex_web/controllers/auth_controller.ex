defmodule CivBotWeb.AuthController do
  @moduledoc """
  Controller for handling OAuth with Discord
  """

  import CivBotWeb.Gettext

  use CivBotWeb, :controller
  plug Ueberauth

  alias Ueberauth.Strategy.Helpers
  alias CivBot.Accounts

  def request(conn, _params) do
    render(conn, "request.html", callback_url: Helpers.callback_url(conn))
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, gettext("You have been logged out"))
    |> configure_session(drop: true)
    |> redirect(to: "/")
  end

  def callback(conn, %{"error" => _error}) do
    conn
    |> put_flash(:error, gettext("Failed to authenticate"))
    |> redirect(to: "/")
  end

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    user =
      auth
      |> Accounts.sign_in_or_sign_up()
      |> case do
        {:ok, u} -> u
        u -> u
      end

    conn =
      conn
      |> put_session(:current_user, user)
      |> configure_session(renew: true)

    case user do
      %Accounts.User{player_name: nil} -> redirect(conn, to: Routes.user_path(conn, :edit))
      _ -> redirect(conn, to: "/")
    end
  end
end
