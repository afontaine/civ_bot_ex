defmodule CivBotWeb.UserController do
  use CivBotWeb, :controller

  alias CivBot.Accounts
  alias CivBot.Accounts.User

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = Accounts.change_user(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.create_user(user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "User created successfully.")
        |> redirect(to: Routes.user_path(conn, :show, user))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, _params) do
    user = get_session(conn, :current_user)
    render(conn, "show.html", user: user)
  end

  def edit(conn, _params) do
    conn
    |> get_session(:current_user)
    |> case do
      nil ->
        redirect(conn, to: "/")

      user ->
        user
        |> Accounts.change_user()
        |> (&render(conn, "edit.html", user: user, changeset: &1)).()
    end
  end

  def update(conn, %{"user" => user_params}) do
    user = get_session(conn, :current_user)

    case Accounts.update_user(user, user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "User updated successfully.")
        |> put_session(:current_user, user)
        |> redirect(to: Routes.user_path(conn, :show))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def delete(conn, _params) do
    user = get_session(conn, :current_user)
    {:ok, _user} = Accounts.delete_user(user)

    conn
    |> put_flash(:info, "User deleted successfully.")
    |> redirect(to: "/")
  end
end
