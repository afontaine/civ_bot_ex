defmodule CivBotWeb.Router do
  use CivBotWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug CivBotWeb.Plugs.Auth
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/auth", CivBotWeb do
    pipe_through :browser

    get "/:provider", AuthController, :request
    get "/:provider/callback", AuthController, :callback
    post "/:provider/callback", AuthController, :callback
    post "/logout", AuthController, :delete
  end

  scope "/", CivBotWeb do
    pipe_through :browser

    get "/", PageController, :index

    scope "/user" do
      get "/", UserController, :show
      get "/edit", UserController, :edit
      put "/update", UserController, :update
      delete "/", UserController, :delete
    end

    # resources "/users", UserController, except: [:create]
    resources "/games", GameController
  end

  # Other scopes may use custom stacks.
  # scope "/api", CivBotWeb do
  #   pipe_through :api
  # end
end
