defmodule CivBot.Games.Game do
  use Ecto.Schema
  import Ecto.Changeset
  alias CivBot.Accounts.User

  schema "games" do
    field :channel_id, :string
    field :game_name, :string
    many_to_many(:users, User, join_through: "users_games", on_replace: :delete)

    timestamps()
  end

  @doc false
  def changeset(game, attrs) do
    game
    |> cast(attrs, [:game_name, :channel_id])
    |> validate_required([:game_name, :channel_id])
  end
end
