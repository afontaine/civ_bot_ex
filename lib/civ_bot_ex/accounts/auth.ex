defmodule CivBot.Accounts.Auth do
  use Ecto.Schema
  import Ecto.Changeset
  alias CivBot.Accounts.Auth
  alias Ueberauth.Auth, as: UAuth

  @primary_key false
  embedded_schema do
    field :uid, :string
    field :nickname, :string
    field :image, :binary
  end

  @doc false
  def create(%UAuth{uid: uid, info: %{nickname: nickname, image: image}}),
    do: create(%{uid: uid, nickname: nickname, image: image})

  def create(%{} = attrs) do
    %Auth{}
    |> cast(attrs, [:uid, :nickname, :image])
    |> validate_required([:uid, :nickname])
    |> apply_action(:insert)
  end
end
