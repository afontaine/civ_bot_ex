defmodule CivBot.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias CivBot.Accounts.Auth
  alias CivBot.Games.Game

  schema "users" do
    field :discord_id, :string
    field :player_name, :string

    many_to_many(:games, Game, join_through: "users_games", on_replace: :delete)

    timestamps()
  end

  @doc false
  def changeset(user, %Auth{uid: id}),
    do: changeset(user, %{discord_id: id})

  def changeset(user, attrs) do
    user
    |> cast(attrs, [:player_name, :discord_id])
    |> validate_required([:discord_id])
  end
end
