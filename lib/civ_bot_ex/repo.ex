defmodule CivBot.Repo do
  use Ecto.Repo,
    otp_app: :civ_bot_ex,
    adapter: Ecto.Adapters.Postgres
end
