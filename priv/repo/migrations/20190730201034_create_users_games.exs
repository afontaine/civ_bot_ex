defmodule CivBot.Repo.Migrations.CreateUsersGames do
  use Ecto.Migration

  def change do
    create table(:users_games, primary_key: false) do
      add(:game_id, references(:games, on_delete: :delete_all), primary_key: true)
      add(:user_id, references(:users, on_delete: :delete_all), primary_key: true)
      timestamps()
    end

    create(index(:users_games, [:game_id]))
    create(index(:users_games, [:user_id]))

    create(unique_index(:users_games, [:user_id, :game_id], name: :user_id_game_id_unique_index))
  end
end
