defmodule CivBot.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :player_name, :string
      add :discord_id, :string

      timestamps()
    end
  end
end
