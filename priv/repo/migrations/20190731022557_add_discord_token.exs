defmodule CivBot.Repo.Migrations.AddDiscordToken do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :discord_token, :string
    end
  end
end
